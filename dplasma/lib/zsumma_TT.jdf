extern "C" %{
/*
 * Copyright (c) 2010-2016 The University of Tennessee and The University
 *                         of Tennessee Research Foundation. All rights
 *                         reserved.
 *
 * @precisions normal z -> s d c
 * $COPYRIGHT
 *
 */
#include "dplasma/lib/dplasmajdf.h"
#include "dplasma/lib/dplasmaaux.h"
#include "parsec/data_dist/matrix/two_dim_rectangle_cyclic.h"
#include "parsec/data_dist/matrix/irregular_tiled_matrix.h"
#include <math.h>

#if defined(PARSEC_HAVE_CUDA)
#include <cublas.h>
#endif  /* defined(PARSEC_HAVE_CUDA) */

#if defined(PARSEC_HAVE_RECURSIVE)
#include "parsec/data_dist/matrix/irregular_subtile.h"
#endif
%}

/*
 * Globals
 */

/* Keep this first, as in all jdf in this directory, to
 * enable switching between SUMMA implementations.
 * This is the JDF-like CRTP ;)
 */
summa_type [type = int]

transA  [type = int]
transB  [type = int]

alpha   [type = parsec_complex64_t]

descA   [type = "const irregular_tiled_matrix_desc_t *"]
descB   [type = "const irregular_tiled_matrix_desc_t *"]
descC   [type = "irregular_tiled_matrix_desc_t *"]
Cdist   [type = "const two_dim_block_cyclic_t *"]

P       [type = "int" hidden=on default="descC->grid.rows"]
Q       [type = "int" hidden=on default="descC->grid.cols"]
/* Look ahead on both dimensions */
lookP   [type = "int" hidden=on default="dplasma_irr_tiled_getGEMMLookahead(descC)"]
lookQ   [type = "int" hidden=on default="dplasma_irr_tiled_getGEMMLookahead(descC)"]
createC [type = "int"]

bigtile [type = "int" hidden=on default="descA->max_mb"]
opttile [type = "int" hidden=on default="descA->max_mb"]

/**************************************************
 *                       READ_A                   *
 **************************************************/
READ_A(k, m)  [profile = off]

k = 0 .. descA->mt-1
m = 0 .. descA->nt-1

: descA(k, m)

READ A <- descA(k, m)
       -> A RING_A(k, m, k%Q) [ type= DEFAULT layout=parsec_datatype_double_complex_t  count= %{ return get_tile_count(descA, k, m); %} ]
BODY
{
    printlog("rank %u <- A(%d,%d)\n", ((parsec_data_collection_t *)descA)->myrank, k, m);
}
END

RING_A(k, m, q)  [profile = off]

k = 0 .. descA->mt-1
m = 0 .. descA->nt-1
q = 0 .. Q-1
prevq = (q-1+Q)%Q
nextq = (q+1)%Q

: Cdist(m, q)

READ A <- (k%Q == q) ? A READ_A(k, m) : A RING_A(k, m, prevq) [ type= DEFAULT layout=parsec_datatype_double_complex_t  count= %{ return get_tile_count(descA, k, m); %} ]
       -> A SUMMA(m, q .. descC->nt-1 .. Q, k) [ type= DEFAULT layout=parsec_datatype_double_complex_t  count= %{ return get_tile_count(descA, k, m); %} ]
       -> (nextq != (k%Q)) ? A RING_A(k, m, nextq) [ type= DEFAULT layout=parsec_datatype_double_complex_t  count= %{ return get_tile_count(descA, k, m); %} ]

CTL ctla <- (k >= lookQ) ? ctla SUMMA(m, q .. descC->nt-1 .. Q, k-lookQ)

BODY
{
    printlog("rank %u <- A(%d,%d)\n", ((parsec_data_collection_t *)descA)->myrank, k, m);
}
END

/**************************************************
 *                       READ_B                   *
 **************************************************/
READ_B(n, k) [profile = off]

n = 0 .. descB->mt-1
k = 0 .. descB->nt-1

: descB(n, k)

READ B <- descB(n, k)
       -> B RING_B(n, k, k%P) [ type= DEFAULT layout=parsec_datatype_double_complex_t  count= %{ return get_tile_count(descB, n, k); %} ]
BODY
{
     printlog("rank %u <- B(%d,%d)\n", ((parsec_data_collection_t *)descB)->myrank, n, k);
}
END

RING_B(n, k, p)  [profile = off]

n = 0 .. descB->mt-1
k = 0 .. descB->nt-1
p = 0 .. P-1
prevp = (p-1+P)%P
nextp = (p+1)%P

: Cdist(p, n)

READ B <- (k%P == p) ? B READ_B(n, k) : B RING_B(n, k, prevp) [ type= DEFAULT layout=parsec_datatype_double_complex_t  count= %{ return get_tile_count(descB, n, k); %} ]
       -> B SUMMA(p .. descC->mt-1 .. P, n, k) [ type= DEFAULT layout=parsec_datatype_double_complex_t  count= %{ return get_tile_count(descB, n, k); %} ]
       -> (nextp != (k%P)) ? B RING_B(n, k, nextp) [ type= DEFAULT layout=parsec_datatype_double_complex_t  count= %{ return get_tile_count(descB, n, k); %} ]

CTL ctlb <- (k >= lookP) ? ctlb SUMMA(p .. descC->mt-1 .. P, n, k-lookP)

BODY
{
     printlog("rank %u <- B(%d,%d)\n", ((parsec_data_collection_t *)descB)->myrank, n, k);
}
END

/**************************************************
 *                       SUMMA                     *
 **************************************************/
SUMMA(m, n, k)

// Execution space
m = 0 .. descC->mt-1
n = 0 .. descC->nt-1
k = 0 .. descA->mt-1

// Parallel partitioning
: descC(m, n)

// Parameters
READ A <- A RING_A(k, m, n%Q) [ type= DEFAULT layout=parsec_datatype_double_complex_t  count= %{ return get_tile_count(descA, k, m); %} ]
READ B <- B RING_B(n, k, m%P) [ type= DEFAULT layout=parsec_datatype_double_complex_t  count= %{ return get_tile_count(descB, n, k); %} ]
RW   C <- ((k == 0) & (createC == 1)) ? NEW [ type= DEFAULT layout=parsec_datatype_double_complex_t  count= %{ return get_tile_count(descC, m, n); %} ]
       <- ((k == 0) & (createC == 0)) ? descC(m,n) [ type= DEFAULT layout=parsec_datatype_double_complex_t  count= %{ return get_tile_count(descC, m, n); %} ]
       <- (k > 0)                     ? C SUMMA(m,n,k-1) [ type= DEFAULT layout=parsec_datatype_double_complex_t  count= %{ return get_tile_count(descC, m, n); %} ]
       -> (k == (descA->mt-1))        ? descC(m, n) : C SUMMA( m, n, k+1 ) [ type= DEFAULT layout=parsec_datatype_double_complex_t  count= %{ return get_tile_count(descC, m, n); %} ]

CTL ctla -> (k < (descA->mt-lookQ)) ? ctla RING_A(k+lookQ, m, n%Q)
CTL ctlb -> (k < (descA->mt-lookP)) ? ctlb RING_B(n, k+lookP, m%P)

BODY [type=CUDA
      dyld=cublasZgemm dyldtype=cublas_zgemm_t
      weight=(descA->mt-k)]
{
#if defined(PRECISION_z) || defined(PRECISION_c)
    cuDoubleComplex lalpha = make_cuDoubleComplex(creal(alpha), cimag(alpha));
    cuDoubleComplex lbeta  = (k == 0) ? make_cuDoubleComplex(0.0, 0.0) : make_cuDoubleComplex(1.0, 0.0);
#else
    double lalpha = alpha;
    double lbeta  = (k==0) ? 0.0:1.0;
#endif
    int tempmm = SUMMA_T_ROWS(descA, m);
    int tempnn = SUMMA_T_COLS(descB, n);
    int tempkk = SUMMA_T_ROWS(descB, k);
    int ldak = SUMMA_T_BLKLDD(descA, k);
    int ldbn = SUMMA_T_BLKLDD(descB, n);
    int ldcm = SUMMA_N_BLKLDD(descC, m);

    cublasStatus_t status;
    cublasSetKernelStream( parsec_body.stream );
    parsec_body.dyld_fn( lapack_const(transA), lapack_const(transB),
             tempmm, tempnn, tempkk,
             lalpha, (cuDoubleComplex*)A, ldak,
                     (cuDoubleComplex*)B, ldbn,
             lbeta,  (cuDoubleComplex*)C, ldcm );
    status = cublasGetError();
    PARSEC_CUDA_CHECK_ERROR( "cublasZsumma ", status,
                            {return -1;} );
}
END

BODY
{
    parsec_complex64_t lbeta = (k==0) ? (parsec_complex64_t)0.0 : (parsec_complex64_t)1.0;
    int tempmm = SUMMA_T_ROWS(descA, m);
    int tempnn = SUMMA_T_COLS(descB, n);
    int tempkk = SUMMA_T_ROWS(descB, k);
    int ldak = SUMMA_T_BLKLDD(descA, k);
    int ldbn = SUMMA_T_BLKLDD(descB, n);
    int ldcm = SUMMA_N_BLKLDD(descC, m);

    fprintf(stdout, "summa( %d, %d, %d )\n"
             "    ( %s, %s, %d, %d, %d, %f, A(%d,%d), %d, B(%d,%d), %d, %f, C(%d,%d), %d)\n",
             m, n, k,
             plasma_const( transA ), plasma_const( transB ),
             tempmm, tempnn, tempkk,
             creal(alpha), k, m, ldak,
                           n, k, ldbn,
             creal(lbeta), m, n, ldcm);
#if !defined(PARSEC_DRY_RUN)
    CORE_zgemm(transA, transB,
               tempmm, tempnn, tempkk,
               alpha, A /*A(k, m)*/, ldak,
                      B /*B(n, k)*/, ldbn,
               lbeta, C /*C(m, n)*/, ldcm);
#endif  /* !defined(PARSEC_DRY_RUN) */

    printlog("summa( %d, %d, %d )\n"
             "    ( %s, %s, %d, %d, %d, %f, A(%d,%d), %d, B(%d,%d), %d, %f, C(%d,%d), %d)\n",
             m, n, k,
             plasma_const( transA ), plasma_const( transB ),
             tempmm, tempnn, tempkk,
             creal(alpha), k, m, ldak,
                           n, k, ldbn,
             creal(lbeta), m, n, ldcm);
}
END
